﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartShop.DAL;

namespace SmartShop.UI.Service
{
    public static class ShoppingOrderSvr
    {
        public static bool Insert(ShoppingOrder obj, List<ShoppingOrderDetail> lstDetail, ref string pOrderCode)
        {
            return ShoppingOrderDAL.Insert(obj, lstDetail, ref pOrderCode);
        }
    }
}