﻿using System.Web.Mvc;
using System.Data;
using SmartShop.UI.Models;

namespace SmartShop.UI.Controllers
{
    public class TrangchuController : Controller
    {
        //
        // GET: /Trangchu/

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult ClientDoSearchProduct(string keyword, int pageSize, int currentPage)
        {
            int totalRecord = 0;
            DataSet result = ProductSvr.GetSanPhamTheoDieuKien(keyword, "", null, null, pageSize, currentPage, ref totalRecord);
            SearchResultModel model = new SearchResultModel();
            model.ProductList = result;
            model.Keyword = keyword;
            model.TotalRecord = totalRecord;
            //string ret = RenderPartialHelper.RenderPartialToString(this.ControllerContext, Url.Content("~/Views/Shared/_QuickSearch.cshtml"), result);
            //return Json(ret, JsonRequestBehavior.AllowGet);
            return PartialView("~/Views/Shared/_SearchResult.cshtml", model);
        }

        public PartialViewResult ShoppingCart()
        {
            return PartialView("~/Views/Shared/_ShoppingCart.cshtml");
        }

    }
}
