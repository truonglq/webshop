﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using SmartShop.DAL;
using SmartShop.UI.Models;
using SmartShop.Utilities;
using SmartShop.UI.Service;

namespace SmartShop.UI.Controllers
{
    public class GioHangController : Controller
    {
        //
        // GET: /GioHang/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddToCart(string productId, int quantity = 0, string imgUrl = null, decimal price = 0)
        {
            var product = ProductSvr.GetByID(int.Parse(productId));
            var cartItem = new GioHangModel();
            if (product != null)
            {
                cartItem.ProductId = product.ProductID;
                cartItem.ProductCode = product.ProductCode;
                cartItem.ProductName = product.ProductName;
                cartItem.ImageUrl = imgUrl;
                cartItem.Quantity = quantity;
                cartItem.Price = price;
            }
            var items = new List<GioHangModel>();
            if (System.Web.HttpContext.Current.Session["ShoppingCart"].IsNullOrEmpty())
            {
                items.Add(cartItem);
                System.Web.HttpContext.Current.Session["ShoppingCart"] = items;
            }
            else
            {
                items = ((List<GioHangModel>)System.Web.HttpContext.Current.Session["ShoppingCart"]);
                var item = items.FirstOrDefault(x => x.ProductId == cartItem.ProductId && x.ProductCode == cartItem.ProductCode);
                if (!item.IsNullOrEmpty())
                    item.Quantity += quantity;
                else
                    items.Add(cartItem);
                System.Web.HttpContext.Current.Session["ShoppingCart"] = items;
            }

            UpdateShoppingCartCookie(items);

            return Json(new
            {
                Success = true,
                Message = "Thêm vào giỏ hàng thành công!"
            });
        }

        [HttpPost]
        public ActionResult RemoveFromCart(string productId)
        {
            var items = ((List<GioHangModel>)System.Web.HttpContext.Current.Session["ShoppingCart"]);

            if (items != null && items.Any())
            {
                var existItem = items.FirstOrDefault(x => x.ProductId == int.Parse(productId));
                if (existItem != null)
                {
                    items.Remove(existItem);
                }
            }
            if (items == null || !items.Any())
            {
                Session.Remove("ShoppingCart");
            }
            UpdateShoppingCartCookie(items);
            return Json(new
            {
                Success = true,
                Message = "Xóa khỏi giỏ hàng thành công!"
            });
        }

        public void UpdateShoppingCartCookie(List<GioHangModel> shoppingCart)
        {
            var cartItems = "";
            if (shoppingCart.Any())
            {
                cartItems = JsonConvert.SerializeObject(shoppingCart, Formatting.None);
            }
            var httpCookie = Response.Cookies["ShoppingCartCookie"];
            if (httpCookie != null)
                httpCookie.Value = cartItems;
        }

        [HttpPost]
        public ActionResult OrderNow(string trueName, string acountLogin, string acountPass, string email, string phone, string address)
        {
            var code = string.Empty;
            var member = new Member
            {
                TrueName = trueName,
                AcountLogin = acountLogin,
                AcountPass = acountPass,
                Email = email,
                Phone = phone,
                Address = address,
                MemberGroupID = (int)Constant.MemberGroupId.Customer,
                Acitve = true
            };

            var memberId = SysUserSvr.Insert(member);
            var items = ((List<GioHangModel>)System.Web.HttpContext.Current.Session["ShoppingCart"]);
            var total = items.Sum(t => t.Price);
            var order = new ShoppingOrder
            {
                MemberID = memberId,
                OrderDate = DateTime.Now,
                RequiredDate = DateTime.Now,
                ShipAddress = address,
                Status = 0,
                Total = (double)total
            };
            var orderDetails = items.Select(x => new ShoppingOrderDetail
            {
                ProductID = x.ProductId,
                Price = (int)x.Price,
                Amount = x.Quantity

            }).ToList();

            var status = ShoppingOrderSvr.Insert(order, orderDetails, ref code);
            if (status)
            {
                System.Web.HttpContext.Current.Session["ShoppingCart"] = null;
                if (Request.Cookies["ShoppingCartCookie"] != null)
                {
                    var myCookie = new HttpCookie("ShoppingCartCookie")
                    {
                        Expires = DateTime.Now.AddDays(-1d)
                    };
                    Response.Cookies.Add(myCookie);
                }
                return Json(new
                {
                    Success = true,
                    Message = "Đặt hàng thành công!"
                });
            }
            return Json(new
            {
                Success = true,
                Message = "Đặt hàng thất bại!"
            });
        }
    }
}
