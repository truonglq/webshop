﻿using SmartShop.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartShop.UI.Controllers
{
    public class TimsanphamController : Controller
    {
        //
        // GET: /Timsanpham/

        public ActionResult Index()
        {
            return View();
        }
        
        public PartialViewResult GetData(TimKiemSanPham model)
        {
            int totalRecord = 0;
            var result = ProductSvr.GetvwProductSet_Product_Paging(model.keyword, model.pageSize, model.currentPage, ref totalRecord);
            return PartialView(result);
        }

    }
}
