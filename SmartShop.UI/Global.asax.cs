﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json;
using SmartShop.UI.Models;

namespace SmartShop.UI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            var shoppingCart = HttpContext.Current.Request.Cookies["ShoppingCartCookie"];
            if (shoppingCart != null)
            {
                var cartItems = JsonConvert.DeserializeObject<List<GioHangModel>>(shoppingCart.Value);
                HttpContext.Current.Session["ShoppingCart"] = cartItems;
            }
            else
            {
                var shoppingCartCookie = new HttpCookie("ShoppingCartCookie")
                {
                    Value = "",
                    Expires = DateTime.Now.AddDays(30)
                };
                Response.Cookies.Add(shoppingCartCookie);
            }
        }
    }
}