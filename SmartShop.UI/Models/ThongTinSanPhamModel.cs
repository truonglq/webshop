﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartShop.UI.Models
{
    public class ThongTinSanPhamModel
    {
        public int ProductID { get; set; }

        public string ProductCode { get; set; }

        public string ProductName { get; set; }

        public string NoSymbolName { get; set; }

        public int ProductSetID { get; set; }

        public int ProductGroupID { get; set; }

        public double Quantity { get; set; }

        public int SizeID { get; set; }

        public int Viewed { get; set; }

        public int Available { get; set; }

        public string THUONG_HIEU { get; set; }

        public string XUAT_XU { get; set; }

        public string BAO_HANH { get; set; }

        public string GIA_BAN { get; set; }

        public string SO_TA { get; set; }

        public string TINH_NANG { get; set; }

        public string ANH_CHINH { get; set; }

        public string ANH_PHU_1 { get; set; }

        public string ANH_PHU_2 { get; set; }

        public string ANH_PHU_3 { get; set; }

        public string ANH_PHU_4 { get; set; }

        public string THONG_SO { get; set; }

        public string TAG { get; set; }

        public string DANH_GIA { get; set; }

        public string CHIET_KHAU { get; set; }
    }

    public class TimKiemSanPham
    {
        public string keyword { get; set; } = null;
        public int pageSize { get; set; } = 10;
        public int currentPage { get; set; } = 1;
    }

    public class TimTin
    {
        public string keyword { get; set; } = null;
        public int catId { get; set; } = 0;
        public int pageSize { get; set; } = 10;
        public int currentPage { get; set; } = 1;
    }
}