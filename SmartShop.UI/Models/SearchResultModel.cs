﻿using System.Collections.Generic;
using SmartShop.DAL;
using System.Data;

namespace SmartShop.UI.Models
{
    public class SearchResultModel
    {
        public string Keyword { get; set; }

        public DataSet  ProductList { get; set; }

        public int TotalRecord { get; set; }
    }
}