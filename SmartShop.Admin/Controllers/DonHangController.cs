﻿using SmartShop.DAL;
using SmartShop.Utilities.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartShop.Admin.Controllers
{
    public class DonHangController : BaseController
    {
        //
        // GET: /DonHang/

        public ActionResult Index()
        {
            List<vwShoppingOrder_Member> data = new List<vwShoppingOrder_Member>();
            return View(data);
        }

        public JsonResult Delete(int id)
        {
            ProductCatSvr.Delete(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Save(ShoppingOrder model)
        {
            if (model.OrderID == 0)
            {
                ShoppingOrderDAL.Insert(model);
                return Json("Thêm mới thành công!", JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (ShoppingOrderDAL.Update(model) > 0)
                {
                    return Json("Cập nhật thành công!", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult GetData(string keyword, int pagesize, int currentpage, string strFromDate, string strToDate, int memberID)
        {
            var data = new List<vwShoppingOrder_Member>();
            try
            {
                int? totalRecord = 0;
                string userName = MemberDAL.UserName;
                var MemberID = 0;
                var ManageBy = 0;
                if (memberID == 0)
                {
                    MemberID = 0;
                    ManageBy = 0;
                }
                else
                {
                    MemberID = memberID;
                    var objMember = MemberDAL.GetByID(MemberID);
                    if (objMember != null)
                    {
                        ManageBy = objMember.ManageBy.Value;
                    }
                }
                DateTime? fromDate = Utility.ConvertToDateTime(strFromDate.Trim());
                DateTime? toDate = Utility.ConvertToDateTime(strToDate.Trim());
                DateTime? OrderDate = null;
                int? Status = null;
                data = ShoppingOrderDAL.GetP_vwShoppingOrder_Member_SearchPaging(userName, ManageBy, keyword, fromDate, toDate, OrderDate, Status, pagesize, currentpage, ref totalRecord);
            }
            catch (Exception ex) {
                Utilities.Common.OutputLog(ex);
            }
            return PartialView(data);
        }

        public JsonResult GetDetail(int id)
        {
            ShoppingOrder data = new ShoppingOrder();
            if (id > 0)
                data = ShoppingOrderDAL.GetByID(id);
            string ret = RenderPartialHelper.RenderPartialToString(this.ControllerContext, Url.Content("~/Views/DonHang/GetDetail.cshtml"), data);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

    }
}
