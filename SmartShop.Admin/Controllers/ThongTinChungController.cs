﻿using SmartShop.Admin.Services;
using SmartShop.DAL;
using SmartShop.Utilities.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartShop.Admin.Controllers
{
    public class ThongTinChungController : BaseController
    {
        //
        // GET: /ThongTinChung/

        public ActionResult Index()
        {
            var data = ThongTinDAL.GetAllItem();//.Where(e => e.GroupInfo != "LOGO" && e.GroupInfo != "SLIDE");
            return View(data);
        }

        public JsonResult Delete(int id)
        {
            ThongTinChunSvr.Delete(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Save(HT_THONG_TIN model)
        {
            if (model.ID == 0)
            {
                ThongTinChunSvr.Insert(model);
                return Json("", JsonRequestBehavior.AllowGet);
            }
            else
            {
                ThongTinChunSvr.Update(model);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult GetData()
        {
            List<HT_THONG_TIN> data = ThongTinChunSvr.GetAllItem().ToList();
            return PartialView(data);
        }

        public JsonResult GetDetail(int id)
        {
            HT_THONG_TIN data = new HT_THONG_TIN();
            if (id > 0)
                data = ThongTinChunSvr.GetById(id);
            string ret = RenderPartialHelper.RenderPartialToString(this.ControllerContext, Url.Content("~/Views/ThongTinChung/GetDetail.cshtml"), data);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

    }
}
