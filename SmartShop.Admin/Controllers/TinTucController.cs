﻿using SmartShop.Admin.Services;
using SmartShop.DAL;
using SmartShop.Utilities;
using SmartShop.Utilities.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartShop.Admin.Controllers
{
    public class TinTucController : BaseController
    {
        //
        // GET: /TinTuc/

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult GetData()
        {
            string keyword = null;
            int? totalRecord = 0;
            var pagesize = 10;
            var currentpage = 1;
            List<Artical_DTO> data = ArticalSvr.GetAllItem();
            return PartialView(data);
        }

        public JsonResult GetDetail(int id)
        {
            Artical data = new Artical();
            if (id > 0)
            {
                data = ArticalSvr.GetByID(id);
            }
            string ret = RenderPartialHelper.RenderPartialToString(this.ControllerContext, Url.Content("~/Views/TinTuc/GetDetail.cshtml"), data);
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            ArticalSvr.Delete(id);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Save(Artical model)
        {
            if (Validate(model))
            {
                if (model.ArticalID == 0)
                {
                    ArticalSvr.Insert(model);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ArticalSvr.Update(model);
                    return Json("1", JsonRequestBehavior.AllowGet);
                }
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }

        private bool Validate(Artical model)
        {
            bool result = true;
            return result;
        }

        [HttpPost]
        public void Upload()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                LFile.WriteFile(file, Server.MapPath("~/Media/News/" + LDateTime.GetCurrentDate().DateToString("yyyyMMdd") + "/"));
            }

        }

    }
}
