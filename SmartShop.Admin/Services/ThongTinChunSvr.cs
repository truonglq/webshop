﻿using SmartShop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartShop.Admin.Services
{
    public static class ThongTinChunSvr
    {
        public static HT_THONG_TIN GetById(int id)
        {
            return ThongTinDAL.GetByID(id);
        }

        public static HT_THONG_TIN Insert(HT_THONG_TIN obj)
        {
            return ThongTinDAL.Insert(obj);
        }

        public static HT_THONG_TIN Update(HT_THONG_TIN obj)
        {
            return ThongTinDAL.Update(obj);
        }

        public static bool Delete(int id)
        {
            return ThongTinDAL.Delete(id);
        }

        public static List<HT_THONG_TIN> GetAllItem()
        {
            return ThongTinDAL.GetAllItem();
        }

        public static List<HT_THONG_TIN> GetByParrent(string pParrentID)
        {
            return ThongTinDAL.GetByParrent(pParrentID);
        }
    }
}