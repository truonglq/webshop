﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using SmartShop.Utilities;
using SmartShop.Utilities.Helper;

namespace SmartShop.DAL
{
    public class ThongTinDAL
    {
        public static HT_THONG_TIN GetByID(int id)
        {
            var dc = new SmartShopEntities();
            return dc.HT_THONG_TIN.SingleOrDefault(c => c.ID == id);
        }

        public static HT_THONG_TIN Insert(HT_THONG_TIN obj)
        {

            HT_THONG_TIN kq = null;
            SmartShopEntities entities = new SmartShopEntities();
            try
            {
                entities.HT_THONG_TIN.Add(obj);
                entities.SaveChanges();
                kq = obj;
            }
            catch (System.Exception ex)
            {
                kq = null;
            }
            finally
            {
                entities = null;
            }
            return kq;
        }

        public static HT_THONG_TIN Update(HT_THONG_TIN obj)
        {
            HT_THONG_TIN kq = null;
            SmartShopEntities entities = new SmartShopEntities();
            try
            {
                var original = entities.HT_THONG_TIN.Find(obj.ID);
                if (original != null)
                {
                    BoHelper.CopyData(obj, original);
                    entities.SaveChanges();
                    kq = obj;
                }
            }
            catch (System.Exception ex)
            {
                kq = null;
            }
            finally
            {
                entities = null;
            }
            return kq;
        }

        public static bool Delete(int id)
        {
            bool kq = true;
            SmartShopEntities entities = new SmartShopEntities();
            HT_THONG_TIN obj = GetByID(id);
            try
            {
                entities.HT_THONG_TIN.Attach(obj);
                entities.HT_THONG_TIN.Remove(obj);
                entities.SaveChanges();
            }
            catch (System.Exception ex)
            {
                kq = false;
            }
            finally
            {
                entities = null;
            }
            return kq;
        }

        public static List<HT_THONG_TIN> GetAllItem()
        {
            try
            {
                var dc = new SmartShopEntities();
                var lst = dc.HT_THONG_TIN.ToList();
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<HT_THONG_TIN> GetRootItem()
        {
            try
            {
                var dc = new SmartShopEntities();
                var lst = dc.HT_THONG_TIN.Where(e => e.GroupInfo == null).ToList();
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<HT_THONG_TIN> GetByParrent(string GroupInfo)
        {
            try
            {
                var dc = new SmartShopEntities();
                var lst = dc.HT_THONG_TIN.ToList();
                if (!GroupInfo.IsNullOrEmpty())
                {
                    lst = (from l in lst
                           where l.GroupInfo == GroupInfo
                           select l).ToList();
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}